const Joi = require('joi');
require('dotenv').config();

const envSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string().required().valid('production', 'development', 'staging', 'test'),
    PORT: Joi.number().default(3001),
    MONGODB_URL: Joi.string().required().description('mongodb url'),
    MONGODB_HOST: Joi.string().optional().description('mongodb host'),
    MONGODB_USER: Joi.string().optional().description('mongodb user'),
    MONGODB_PASS: Joi.string().optional().description('mongodb password'),
  })
  .unknown();

const { value, error } = envSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) throw new Error(`Config validation error: ${error}`);

module.exports = {
  env: value.NODE_ENV,
  port: value.PORT,
  mongoose: {
    url: value.MONGODB_URL,
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
};
