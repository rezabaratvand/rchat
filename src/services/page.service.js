'use strict';

const httpStatus = require('http-status');
const { Page } = require('../models/index.model');
const ApiError = require('../utils/api-error.util');
const { userService } = require('./index.service');
const moment = require('moment');

const pagePopulation = {
  path: 'pageViews.user',
  model: 'User',
  select: ['name'],
};

module.exports = {
  async createPage(pageBody) {
    const { pageLink } = pageBody;
    const pageExists = await Page.findOne({ pageLink });
    if (pageExists) throw new ApiError(httpStatus.CONFLICT, 'the page already exists');

    return Page.create(pageBody);
  },

  findAllPages() {
    return Page.find();
  },

  async findPageById(pageId) {
    const page = await Page.findById(pageId);
    if (!page) throw new ApiError(httpStatus.NOT_FOUND, `page '${pageId}' not found`);
    return page;
  },

  //* pave view methods
  async createPageView(pageId, pageViewBody) {
    const { user, pageLeaveTime, date } = pageViewBody;
    // validate page
    const page = await this.findPageById(pageId);
    // validate user
    await userService.findUserById(user);

    // check if the user has visited this page on a given date
    const pvExists = page.pageViews.find(
      pv =>
        pv.user == user && moment(pv.date).format('YYYY-MM-DD') == moment(date).format('YYYY-MM-DD')
    );

    if (pvExists) {
      pvExists.pageLeaveTime = pageLeaveTime;
      const body = { pageEnterTime: pvExists.pageEnterTime, pageLeaveTime };
      pvExists.viewTime = this.calculateViewTime(body);
      await page.save();

      return page.pageViews;
    }

    page.viewCount++;
    page.pageViews.push({ viewTime, ...pageViewBody });
    await page.save();

    return page.pageViews;
  },

  getPageStatistic() {
    return Page.aggregate([
      // Stage 1: destruct pageViews array
      {
        $unwind: { path: '$pageViews' },
      },
      // Stage 2: group the result by pageLink
      {
        $group: {
          _id: '$pageLink',
          numberOfViews: { $sum: 1 },
          maxViewTime: { $max: '$pageViews.viewTime' },
          minViewTime: { $min: '$pageViews.viewTime' },
          // previousPageLink: { $}
        },
      },
      // Stage 3: sort the result of previous stage by numberOfViews (DESC)
      {
        $sort: { numberOfViews: -1 },
      },
      // Stage 4: add new field to rename id to pageLink
      {
        $addFields: { pageLink: '$_id' },
      },
      // Stage 5: hide id field
      {
        $project: { _id: 0 },
      },
    ]);
  },

  calculateViewTime(pageViewBody) {
    let { pageEnterTime, pageLeaveTime } = pageViewBody;
    pageEnterTime = moment(pageEnterTime, 'HH:mm');
    pageLeaveTime = moment(pageLeaveTime, 'HH:mm');
    const viewTime = moment(pageLeaveTime).diff(pageEnterTime, 'minutes');

    return viewTime;
  },
};
