'use strict';

const httpStatus = require('http-status');
const { User } = require('../models/index.model');
const ApiError = require('../utils/api-error.util');
module.exports = {
  createUser(userBody) {
    return User.create(userBody);
  },
  findAllUsers() {
    return User.find();
  },
  async findUserById(userId) {
    const user = await User.findById(userId);
    if (!user) throw new ApiError(httpStatus.NOT_FOUND, 'user not found');
    return user;
  },

  async updateUserById(userId, updateBody) {
    const user = await this.findUserById(userId);
    Object.assign(user, updateBody);
    await user.save();
    return user;
  },

  async deleteUserById(userId) {
    const user = await this.findUserById(userId);
    await user.deleteOne();
    return 'user deleted successfully';
  },
};
