'use strict';

const { Router } = require('express');
const userRoute = require('./user.route');
const pageRoute = require('./page.route');
// define routes
const router = Router();
router.use('/users', userRoute);
router.use('/pages', pageRoute);

module.exports = router;
