'use strict';

const { Router } = require('express');
const validate = require('../middlewares/validate');
const { userValidation } = require('../validations/index.validation');
const { userController } = require('../controllers/index.controller');

const router = Router();

router
  .route('/')
  .post(validate(userValidation.createUser), userController.createUser)
  .get(userController.findAllUsers);

module.exports = router;
