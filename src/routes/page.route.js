'use strict';

const { Router } = require('express');
const validate = require('../middlewares/validate');
const { pageValidation, pageViewValidation } = require('../validations/index.validation');
const { pageController } = require('../controllers/index.controller');

const router = Router();

router
  .route('/')
  .post(validate(pageValidation.createPage), pageController.createPage)
  .get(pageController.findAllPages);

//* page view routes
router
  .route('/:pageId/views')
  .post(validate(pageViewValidation.createPageView), pageController.createPageView);

router.route('/stats').get(pageController.getPageStatistic);

router.route('/:pageId').get(validate(pageValidation.findPageById), pageController.findPageById);

module.exports = router;
