'use strict';

const catchAsync = require('../utils/catch-async.util');
const { userService } = require('../services/index.service');
const httpStatus = require('http-status');

module.exports = {
  createUser: catchAsync(async (req, res) => {
    const user = await userService.createUser(req.body);
    res.status(httpStatus.CREATED).send({ data: user });
  }),

  findAllUsers: catchAsync(async (req, res) => {
    const users = await userService.findAllUsers();
    res.status(httpStatus.OK).send({ data: users });
  }),

  findUserById: catchAsync(async (req, res) => {
    const user = await userService.findUserById(req.params.userId);
    res.status(httpStatus.OK).send({ data: user });
  }),

  updateUserById: catchAsync(async (req, res) => {
    const user = await userService.updateUserById(req.params.userId, req.body);
    res.status(httpStatus.OK).send({ data: user });
  }),

  deleteUserById: catchAsync(async (req, res) => {
    const result = await userService.deleteUserById(req.params.userId);
    res.status(httpStatus.NO_CONTENT).send({ data: result });
  }),
};
