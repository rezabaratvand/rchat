'use strict';

const httpStatus = require('http-status');
const catchAsync = require('../utils/catch-async.util');
const { pageService } = require('../services/index.service');

module.exports = {
  createPage: catchAsync(async (req, res) => {
    const result = await pageService.createPage(req.body);
    res.status(httpStatus.CREATED).send(result);
  }),

  findAllPages: catchAsync(async (req, res) => {
    const result = await pageService.findAllPages();
    res.status(httpStatus.OK).send({ resultCount: result.length, result });
  }),

  findPageById: catchAsync(async (req, res) => {
    const result = await pageService.findPageById(req.params.pageId);
    res.status(httpStatus.OK).send(result);
  }),

  //* page view methods
  createPageView: catchAsync(async (req, res) => {
    const result = await pageService.createPageView(req.params.pageId, req.body);
    res.status(httpStatus.OK).send(result);
  }),

  getPageStatistic: catchAsync(async (req, res) => {
    const result = await pageService.getPageStatistic();
    res.status(httpStatus.OK).send({ resultCount: result.length, result });
  }),
};
