'use strict';

const Joi = require('joi');

module.exports = {
  createUser: {
    body: Joi.object().keys({
      username: Joi.string().min(5).max(255).required(),
      email: Joi.string().email(),
      name: Joi.string().min(3).max(255).required(),
      age: Joi.number().min(1).max(200),
    }),
  },
};
