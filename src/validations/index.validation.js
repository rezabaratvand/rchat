'use strict';

module.exports.userValidation = require('./user.validation');
module.exports.pageValidation = require('./page.validation');
module.exports.pageViewValidation = require('./page-view.validation');
