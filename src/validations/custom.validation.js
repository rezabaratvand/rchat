'use strict';
const mongoose = require('mongoose');

module.exports = {
  hourMinute(value, helper) {
    if (!value.match(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)) {
      return helper.message(`"{{#label}}"must be a time HH:MM 24-hour format`);
    }
    return value;
  },

  objectId(value, helper) {
    if (!mongoose.isValidObjectId(value)) {
      return helper.message(`"{{#label}}" must be a valid mongo Id`);
    }
    return value;
  },
};
