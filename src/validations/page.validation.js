'use strict';

const Joi = require('joi');
const { objectId } = require('./custom.validation');

module.exports = {
  createPage: {
    body: Joi.object().keys({
      pageLink: Joi.string().required(),
    }),
  },
  findPageById: {
    params: Joi.object().keys({
      pageId: Joi.string().custom(objectId).required(),
    }),
  },
};
