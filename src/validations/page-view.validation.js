'use strict';

const Joi = require('joi');
const { objectId, hourMinute } = require('./custom.validation');

module.exports = {
  createPageView: {
    params: Joi.object().keys({
      pageId: Joi.string().custom(objectId).required(),
    }),

    body: Joi.object().keys({
      user: Joi.string().custom(objectId).required(),
      previousPage: Joi.string().required(),
      pageEnterTime: Joi.string().custom(hourMinute).required(),
      pageLeaveTime: Joi.string().custom(hourMinute).required(),
      date: Joi.date().required(),
    }),
  },
};
