'use strict';

const express = require('express');
const routes = require('./routes/index.route');
const ApiError = require('./utils/api-error.util');
const errorHandler = require('./middlewares/error-handler.middleware');
const mongoSanitize = require('express-mongo-sanitize');
const compression = require('compression');
const xss = require('xss-clean');
const helmet = require('helmet');
const httpStatus = require('http-status');

const app = express();

app.use(express.json());
// http headers security
app.use(helmet());
// sanitize incoming data
app.use(xss());
app.use(mongoSanitize());

app.use(compression());

app.use('/api', routes);

// send back a 404 error for unknown apis
app.use((req, res, next) => {
  throw new ApiError(httpStatus.NOT_FOUND, 'Route not defined');
});

app.use(errorHandler);

module.exports = app;
