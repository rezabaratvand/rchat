'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pageViewSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    index: true,
  },
  previousPage: {
    type: String,
    required: true,
    trim: true,
  },
  // format: HH:MM
  pageEnterTime: {
    type: String,
    required: true,
  },
  // format: HH:MM
  pageLeaveTime: {
    type: String,
    require: true,
  },
  date: {
    type: Date,
    required: true,
  },
  // format: Minutes
  viewTime: {
    type: Number,
    required: true,
  },
});

const pageSchema = new Schema(
  {
    // current page link
    pageLink: {
      type: String,
      required: true,
      trim: true,
      index: true,
    },
    // virtual property
    viewCount: {
      type: Number,
      default: 0,
    },
    pageViews: [pageViewSchema],
  },
  { timestamps: true, versionKey: false }
);

const Page = mongoose.model('Page', pageSchema);

module.exports = Page;
