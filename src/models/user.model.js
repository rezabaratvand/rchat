'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 255,
      trim: true,
      index: true,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      index: true,
    },
    name: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 255,
    },
    age: {
      type: Number,
      required: false,
      min: 1,
      max: 200,
    },
  },
  { timestamps: true, versionKey: false }
);

const User = mongoose.model('User', userSchema);
module.exports = User;
