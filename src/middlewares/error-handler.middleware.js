'use strict';

const httpStatus = require('http-status');
const ApiError = require('../utils/api-error.util');

const errorHandler = (err, req, res, next) => {
  if (!(err instanceof ApiError)) {
    const statusCode = err.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
    const message = err.message || httpStatus[statusCode];
    err = new ApiError(statusCode, message, false, err.stack);
  }
  next(err);

  let { statusCode, message, stack } = err;
  const response = {
    error: true,
    timestamps: new Date(),
    message,
    stack,
  };

  res.status(statusCode).send(response);
};

module.exports = errorHandler;
