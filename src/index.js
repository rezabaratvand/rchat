'use strict';

require('dotenv').config();
const mongoose = require('mongoose');
const app = require('./app');
const config = require('../config/config');

// mongoose configuration
mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
  app.listen(config.port, () => {
    console.log('mongodb started successfully ...');
    console.log(`server is listening on port ${config.port} ...`);
  });
});
